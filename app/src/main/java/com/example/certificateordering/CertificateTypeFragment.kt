package com.example.certificateordering

import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.certificateordering.databinding.FragmentCertificateTypeBinding
import com.example.certificateordering.models.Certificate
import com.example.certificateordering.models.User
import java.text.SimpleDateFormat

class CertificateTypeFragment : Fragment() {
    lateinit var binding: FragmentCertificateTypeBinding
    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        if (arguments != null) {
            user = requireArguments().getSerializable(USER) as User?
        }
        enterTransition = inflater.inflateTransition(R.transition.slide_right)
        exitTransition = inflater.inflateTransition(R.transition.fade)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCertificateTypeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.button.isEnabled = false
        val strArr = mutableListOf<String>()
        val format = SimpleDateFormat("dd.MM.yyy")

        for (i in 0..user?.educations?.size!! - 1) {
            strArr.add(
                format.format(user!!.educations[i].begda).toString()
                        + " - " + format.format(user!!.educations[i].endda).toString()
                        + " | " + user!!.educations[i].dict_faculties.dv
                        + ": " + user!!.educations[i].dict_edu_directions.code
                        + " - " + user!!.educations[i].dict_edu_directions.fdv
            )
        }

        createDropdown(
            binding.chooseStudy,
            binding.studyChoose,
            strArr
        )
        createDropdown(
            binding.chooseCertificateType,
            binding.certificateTypeChoose,
            resources.getStringArray(R.array.certificate_type).toMutableList()
        )

        val certificate = Certificate(
            certificate_type = binding.certificateTypeChoose.text as String,
            certificate_subtype = binding.certificateSubtypeChoose.text as String
        )


        binding.button.setOnClickListener {
            val fragment = CertificateSubtypeFragment()
            val args = Bundle()
            args.putSerializable(USER, user)
            args.putSerializable(CERTIFICATE, certificate)
            fragment.arguments = args
            parentFragmentManager.beginTransaction().addToBackStack(null)
                .replace(R.id.content_body, fragment).commit()
        }
    }

    companion object {
        val USER = "user"
        val CERTIFICATE = "certificate"

        @JvmStatic
        fun newInstance(user: User): CertificateTypeFragment {
            val fragment = CertificateTypeFragment()
            val args = Bundle()
            args.putSerializable(USER, user)
            fragment.arguments = args
            return fragment
        }
    }

    private fun createDropdown(
        linearLayout: LinearLayout,
        mainTextView: TextView,
        strings: MutableList<String>
    ) {
        println(strings.size)
        if (strings.size < 1) {
            mainTextView.text = binding.certificateTypeChoose.text
            mainTextView.isSelected = true
            mainTextView.isActivated = false
            mainTextView.isEnabled = false
        }
        if (strings.size == 1) {
            mainTextView.text = strings[0]
            mainTextView.isSelected = true
            mainTextView.isActivated = false
            mainTextView.isEnabled = false

        } else {
            for (str in strings) {
                val dp: Float = resources.displayMetrics.density
                val textView: TextView = TextView(activity)
                textView.text = str
                textView.setTextAppearance(R.style.dropdown_item)
                textView.setPadding(
                    (10 * dp).toInt(),
                    (12 * dp).toInt(),
                    (10 * dp).toInt(),
                    (12 * dp).toInt()
                )

                textView.setOnClickListener {
                    if (mainTextView == binding.certificateTypeChoose) {
                        binding.chooseCertificateSubtype.removeAllViews()
                        val subTypeArray = mutableListOf<String>()
                        subTypeArray.clear()
                        if (textView.text == resources.getStringArray(R.array.certificate_type)[0]) {
                            for (str in resources.getStringArray(R.array.certificate_subtype)) {
                                subTypeArray.add(str)
                            }
                            binding.certificateSubtypeChoose.isEnabled = true
                            binding.certificateSubtypeChoose.isSelected = false
                            binding.certificateSubtypeChoose.isActivated = false
                            binding.certificateSubtypeChoose.text = "Выберите значение"
                            binding.button.isEnabled = false

                        } else {
                            binding.button.isEnabled = true
                            subTypeArray.add(textView.text as String)
                        }
                        createDropdown(
                            binding.chooseCertificateSubtype,
                            binding.certificateSubtypeChoose,
                            subTypeArray
                        )
                    }
                    if (mainTextView == binding.certificateSubtypeChoose){
                        binding.button.isEnabled = true
                    }
                    mainTextView.text = textView.text
                    mainTextView.isSelected = true
                    mainTextView.isActivated = false
                    linearLayout.visibility = View.GONE
                }
                linearLayout.addView(textView)
            }
        }

        mainTextView.setOnClickListener {
            if (linearLayout.visibility == View.VISIBLE) {
                mainTextView.isActivated = false
                linearLayout.visibility = View.GONE
            } else {
                if (mainTextView == binding.studyChoose) {
                    binding.chooseCertificateType.visibility = View.GONE
                    binding.certificateTypeChoose.isActivated = false
                    binding.chooseCertificateSubtype.visibility = View.GONE
                    binding.certificateSubtypeChoose.isActivated = false
                }
                if (mainTextView == binding.certificateTypeChoose) {
                    binding.chooseStudy.visibility = View.GONE
                    binding.chooseCertificateSubtype.visibility = View.GONE
                    binding.studyChoose.isActivated = false
                    binding.certificateSubtypeChoose.isActivated = false
                }
                if (mainTextView == binding.certificateSubtypeChoose) {
                    binding.chooseStudy.visibility = View.GONE
                    binding.studyChoose.isActivated = false
                    binding.chooseCertificateType.visibility = View.GONE
                    binding.certificateTypeChoose.isActivated = false

                }
                mainTextView.isActivated = true
                linearLayout.visibility = View.VISIBLE
            }
        }
    }
}