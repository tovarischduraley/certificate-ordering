package com.example.certificateordering

import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.certificateordering.databinding.FragmentCardsBinding
import com.example.certificateordering.models.User

class OptionCard(
    var title: String,
    var content: String,
    var identifier: Int,
    val fragment: Fragment?
) {}

class CardsFragment : Fragment() {
    lateinit var binding: FragmentCardsBinding
    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        if (arguments != null){
            user = requireArguments().getSerializable(USER) as User?
        }
            enterTransition = inflater.inflateTransition(R.transition.slide_right)
        exitTransition = inflater.inflateTransition(R.transition.fade)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCardsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        for (card in getCards()) {
            val optionCardLayout = layoutInflater.inflate(R.layout.option_card, null, false)
            optionCardLayout.layoutParams = getCardParams()
            val optionCardImage: ImageView = optionCardLayout.findViewById(R.id.card_image)
            val optionCardTitle: TextView = optionCardLayout.findViewById(R.id.card_title)
            val optionCardContent: TextView = optionCardLayout.findViewById(R.id.card_content)
            optionCardTitle.text = card.title
            optionCardContent.text = card.content
            optionCardImage.setImageResource(card.identifier)
            if (card.fragment != null) {
                optionCardLayout.setOnClickListener {
                    val args = Bundle()
                    args.putSerializable(USER, user)
                    card.fragment.arguments = args
                    parentFragmentManager.beginTransaction().addToBackStack(null)
                        .replace(R.id.content_body, card.fragment).commit()
                }
            }
            binding.cardsFragment.addView(optionCardLayout)
        }
    }

    fun getCardParams(): RelativeLayout.LayoutParams {
        val dp: Float = resources.displayMetrics.density
        val cardLayoutParams = RelativeLayout.LayoutParams(
            (320 * dp).toInt(),
            (101 * dp).toInt()
        )
        cardLayoutParams.setMargins(0, (15 * dp).toInt(), 0, (10 * dp).toInt())

        return cardLayoutParams
    }

    fun getCards(): ArrayList<OptionCard> {
        val cards = ArrayList<OptionCard>()
        cards.add(
            OptionCard(
                "Заказать справку",
                "Заказ необходимых документов по месту востребования",
                resources.getIdentifier(
                    "card1_image_selector",
                    "drawable",
                    "com.example.certificateordering"
                ),
                CertificateTypeFragment()
            )
        )
        cards.add(
            OptionCard(
                "История заказов",
                "Список всех заказанных Вами документов",
                resources.getIdentifier(
                    "card2_image_selector",
                    "drawable",
                    "com.example.certificateordering"
                ),
                null
            )
        )
        return cards
    }

    companion object {
        private val USER = "user"

        fun newInstance(user: User): CardsFragment {
            val fragment = CardsFragment()
            val args = Bundle()
            args.putSerializable(USER, user)
            fragment.arguments = args
            return fragment
        }
    }
}