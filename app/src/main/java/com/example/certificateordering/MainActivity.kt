package com.example.certificateordering

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.certificateordering.databinding.ActivityMainBinding
import com.example.certificateordering.models.User
import com.google.gson.GsonBuilder
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private lateinit var mTextView: TextView
    private val client = OkHttpClient()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.statusBar.backArrow.setOnClickListener {
            onBackPressed()
        }

        val request = Request.Builder()
            .url("https://lk.etu.ru/api/profile/0")
            .header("User-Agent", "OkHttp Headers.java")
            .addHeader("Accept", "application/json, text/plain, */*")
            .addHeader(
                "Cookie",
                    Config().COOKIE
            )
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {

                val body = JSONObject(response.body?.string()).toString()
                val gson = GsonBuilder().create()
                val user = gson.fromJson(body, User::class.java)
                openFragment(CardsFragment.newInstance(user))
            }
        })
    }


    fun openFragment(f: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.content_body, f).commit()
    }

}