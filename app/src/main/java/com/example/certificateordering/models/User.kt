package com.example.certificateordering.models

import java.io.Serializable

data class User(
    val fio: String,
    val educations: List<Education>
): Serializable