package com.example.certificateordering.models

data class EducationDirection (
    val code: String,
    val fdv: String,
)