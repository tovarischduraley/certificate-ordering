package com.example.certificateordering.models
import java.io.Serializable

data class Certificate (
    val certificate_type: String,
    val certificate_subtype: String
) : Serializable