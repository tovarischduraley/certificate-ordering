package com.example.certificateordering.models

data class Faculty(
    val dv: String,
    val fdv: String
)
