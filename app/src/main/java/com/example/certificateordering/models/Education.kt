package com.example.certificateordering.models
import com.google.gson.annotations.SerializedName
import java.util.*

data class Education(
    val begda: Date,
    val endda: Date,
    val dict_edu_directions: EducationDirection,
    val dict_faculties: Faculty,
    val student_card_number: String,
    )