package com.example.certificateordering

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.certificateordering.databinding.FragmentCertificateSubtypeBinding
import com.example.certificateordering.databinding.FragmentCertificateTypeBinding
import com.example.certificateordering.models.Certificate
import com.example.certificateordering.models.User
import com.google.android.material.textfield.TextInputEditText

class CertificateSubtypeFragment : Fragment() {
    lateinit var binding: FragmentCertificateSubtypeBinding
    private var user: User? = null
    private var certificate: Certificate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        if (arguments != null) {
            user = requireArguments().getSerializable(USER) as User?
            certificate = requireArguments().getSerializable(CERTIFICATE) as Certificate?
        }
        enterTransition = inflater.inflateTransition(R.transition.slide_right)
        exitTransition = inflater.inflateTransition(R.transition.fade)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCertificateSubtypeBinding.inflate(layoutInflater)
        return binding.root
    }

    private fun setStyle(textInput: TextInputEditText) {
        textInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    textInput.isActivated = false
                }
                if (!s.isNullOrBlank()) {
                    textInput.isActivated  = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setStyle(binding.textInput1)
        binding.textInput1.setText(user?.fio)
        binding.textInput1.isEnabled = false

        setStyle(binding.textInput2)
//        binding.textInput2.setText(user.educations.)
        binding.textInput2.isEnabled = false

        setStyle(binding.textInput3)

        setStyle(binding.textInput4)
        binding.textInput4.setText(certificate?.certificate_type)
        binding.textInput4.isEnabled = false

        setStyle(binding.textInput5)
        binding.textInput5.setText(certificate?.certificate_subtype)
        binding.textInput5.isEnabled = false

        setStyle(binding.textInput6)

        setStyle(binding.textInput7)

    }

    companion object {
        val USER = "user"
        val CERTIFICATE = "certificate"

        @JvmStatic
        fun newInstance(user: User, certificate: Certificate): CertificateSubtypeFragment {
            val fragment = CertificateSubtypeFragment()
            val args = Bundle()
            args.putSerializable(USER, user)
            args.putSerializable(CERTIFICATE, certificate)
            fragment.arguments = args
            return fragment
        }
    }
}